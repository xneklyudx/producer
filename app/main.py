import time


class Producer:
    @classmethod
    def send_message(cls):
        print("Message send")
        return True

    def run(self):
        while True:
            self.send_message()
            time.sleep(1)


if __name__ == '__main__':
    Producer().run()
