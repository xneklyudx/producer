from app.main import Producer


class TestProducer:
    def test_send_message(self):
        return_value = Producer.send_message()
        assert return_value is True
